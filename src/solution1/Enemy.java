package solution1;

public interface Enemy {
	public String attack();
	public String showDetail();
	public String followPlayer();
}
