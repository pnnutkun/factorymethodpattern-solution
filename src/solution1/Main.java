package solution1;

public class Main {
	public static void main(String[] args) {
		Spawn zombieSpawn = new zombieSpawn();
		Spawn UFOspawn = new UFOspawn();
		Enemy UFO = UFOspawn.spawn();
		Enemy zombie = zombieSpawn.spawn();
		System.out.println("UFO");
		System.out.println(UFO.attack());
		System.out.println(UFO.followPlayer());
		System.out.println(UFO.showDetail());
		System.out.println("======");
		System.out.println("Zombie");
		System.out.println(zombie.attack());
		System.out.println(zombie.followPlayer());
		System.out.println(zombie.showDetail());
	}
}
