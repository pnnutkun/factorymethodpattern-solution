package solution2;

public interface BakeryFactory {
	public Dessert bake();
}
