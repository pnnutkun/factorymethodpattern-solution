package solution2;

public interface Dessert {
	public String showDetail();
	public String showPrice();
}
